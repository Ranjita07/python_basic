# # combined two list in one dict

# # list_key = ["ram","shyam","hari"]
# # list_value = ["ktm","lalitpur","pokhara"]
# # ans = dict(zip(list_key,list_value))
# # print(ans)

# # making value of one dict to the key of another
# dict_one = {"a":"apple"}
# dict_sec = {"d":"doll"}

# swappeddict={}
# swap={}

# for key,val in dict_one.items():
#     for key1 ,val1 in dict_sec.items():
#         swappeddict[key1] =val
#         swap[val1] =key
#     print( swappeddict)
#     print(swap)

d = {1: 10, 2: 20, 3: 30, 4: 40, 5: 50, 6: 60}


def is_key_present(x):
    if x in d:
        print()
    else:
        print('Key is not present in the dictionary')


is_key_present(5)
is_key_present(9)
