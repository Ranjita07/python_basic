import random

user_count = 0
computer_count = 0

while True:
    user_input = input("Enter choice (scissor,paper,rock): ")
    possibility = ["rock", "paper", "scissor"]
    computer_input = random.choice(possibility)
    print(computer_input)
    cw = "computer win"
    uw = "You win"

    if user_input == 'scissors' and computer_input == 'rock' or \
        user_input == 'paper' and computer_input == 'scissors' or \
        user_input == 'rock' and computer_input == 'paper':
        print(cw)
        computer_count += 1

    elif user_input == 'rock' and computer_input == 'scissors' or \
        user_input == 'scissors' and computer_input == 'paper' or \
        user_input == 'paper' and computer_input == 'rock':
        print(uw)
        user_count += 1

    else:
        if user_input == computer_input:
            print('Its a draw!')
            computer_count += 1
            user_count += 1

    print(f'Computer Score: {computer_count} - Your Score: {user_count}')
    print()

# if computer == 'user_input':
#     print("Tie")
# elif computer == 'rock':
#     if user_input == 'scissor':
#         print("you lose")
#     print("you win")

# elif computer == 'scissor':
#     if user_input == 'paper':
#         print("scissor cut the paper! you lose")
#     print("you win")

# elif computer == 'paper':
#     if user_input == 'rock':
#         print("you win")
#     print("you lose")
# else:
#     print("not same")

# if computer == 'user_input':
#     print("Tie")

# elif computer == 'rock' and user_input == 'scissor':
#     print(cw)
#     computer_count += 1
# elif computer == 'paper' and user_input == 'rock':
#     print(cw)
#     computer_count += 1
# elif computer == 'scissor' and user_input == 'paper':
#     print(cw)
#     computer_count += 1
# else:
#     print("you lose")
