import os

parent_path = "/home/ranjita/practice/myFolder"
count_jpg = 0
count_txt = 0
count_jpeg = 0
count_png = 0


for root, dir, items in os.walk(parent_path):
    new_count = len(items)
    path_splitted = root.split('/')
    child_name = path_splitted[-1]

    if child_name in ("child2", "child1", "child3"):
        print(child_name)
        count_jpg = 0
        count_txt = 0
        count_jpeg = 0
        count_png = 0

        for i in items:
            x = i.split('.')
            ext = x[-1]

            if ext == 'jpg':
                count_jpg += 1
            if ext == 'txt':
                count_txt += 1
            if ext == 'jpeg':
                count_jpeg += 1
            if ext == 'png':
                count_png += 1
        print("count_jpg:", count_jpg)
        print("count_txt:", count_txt)
        print("count_jpeg:", count_jpeg)
        print("count_png:", count_png)
