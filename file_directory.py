import os

# os.makedirs('Parent/Child1')
parent_path = '/home/ranjita/practice/Parent'

count_txt = 0
count_jpg = 0
count_jpeg = 0
count_png = 0

for i in os.listdir(parent_path):
    # print(i)
    child_path = os.path.join(parent_path, i)
    count_txt = 0
    count_jpeg = 0
    count_jpg = 0
    count_png = 0
    for j in os.listdir(child_path):
        # print(j)
        x = j.split('.')
        # print(x)
        if x[-1] == 'txt':
            count_txt += 1
        elif x[-1] == 'jpg':
            count_jpg += 1
        elif x[-1] == 'jpeg':
            count_jpeg += 1
        elif x[-1] == 'png':
            count_png += 1
    print("count_png file:", count_png)
    print("count_text file:", count_txt)
    print("count_jpg:", count_jpg)
    print("count_jpeg:", count_jpeg)
